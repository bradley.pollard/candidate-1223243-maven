# Candidate-1223243

## Technologies Used

* Serenity (Cucumber framework)
* Maven
* Rest Assured
* Hamcrest
   

## To Run

* Run `./zalenium-starter.sh`

or 
* Run `CucumberTestSuite`

or 
* `mvn verify`

## Evidence

![alt text](Passing-Tests.png "Passing Tests")
