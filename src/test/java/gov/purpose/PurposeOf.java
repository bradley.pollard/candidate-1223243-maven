package gov.purpose;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;

public class PurposeOf extends UIInteractionSteps {

    @Step("Select purpose of visit")
    public void govUkSelectPurposeOfVisit(String purpose) {

        String selector = "input[value='" + purpose.toLowerCase() + "']";

        By SELECT_PURPOSE_RADIO_BUTTON = By.cssSelector(selector);

        $(SELECT_PURPOSE_RADIO_BUTTON).click();
    }
}
