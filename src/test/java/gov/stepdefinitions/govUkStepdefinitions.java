package gov.stepdefinitions;


import gov.nationality.Nationality;
import gov.navigation.NavigateTo;
import gov.purpose.PurposeOf;
import gov.search.SearchFor;
import gov.staying.StayingFor;
import gov.travelling.TravellingWith;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.cucumber.suiteslicing.SerenityTags;
import net.thucydides.core.annotations.Steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class govUkStepdefinitions {

    @Before
    public void before() {
        SerenityTags.create().tagScenarioWithBatchingInfo();
    }

    @Steps
    private
    NavigateTo navigateTo;

    @Steps
    private
    SearchFor searchFor;

    @Steps
    private
    Nationality nationality;

    @Steps
    private
    PurposeOf purposeOf;

    @Steps
    private
    TravellingWith travellingWith;

    @Steps
    private
    StayingFor stayingFor;
    
    @Given("I search for gov uk visa checking service")
    public void iSearchForGovUkVisaCheckingService() {
        navigateTo.govUkHomePage();
        searchFor.govUkSelectAndSearch();
        navigateTo.govUkClickFirstResult();
    }

    @When("I provide a nationality of {}")
    public void iProvideANationalityOf(String nationalitySelected) {
        navigateTo.govUkSelectStartSearch();
        nationality.govUkSelectNationality(nationalitySelected.replace('"', ' ').trim());
        navigateTo.govUkSelectNextStep();
    }

    @And("I select the reason {}")
    public void iSelectTheReason(String purpose) {
        purposeOf.govUkSelectPurposeOfVisit(purpose.toLowerCase().replace('"', ' ').trim());
        navigateTo.govUkSelectNextStep();
    }

    @And("I state I am intending to stay for {} months")
    public void iStateIAmIntendingToStayForMonths(int length) {
        if (length == -1) {
            System.out.println("Not applicable");
        } else {
            stayingFor.govUkSelectLengthOfStay(length);
            navigateTo.govUkSelectNextStep();
        }
    }

    @And("I state {} to travelling with a partner or visiting a partner or family")
    public void iStateIAmNotTravellingOrVisitingAPartnerOrFamily(String statement) {
        if (statement.equals("NA")) {
            System.out.println("Not applicable");
        } else {
            travellingWith.govUkSelection(statement.toLowerCase().replace('"', ' ').trim());
            navigateTo.govUkSelectNextStep();
        }
    }

    @Then("I will be informed {}")
    public void iWillBeInformed(String decision) {
        assertThat(navigateTo.govUkGetDecision(), equalTo(decision.replace('"', ' ').trim()));
    }
}
