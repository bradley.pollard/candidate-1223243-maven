package gov.navigation;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;

public class NavigateTo extends UIInteractionSteps {

    private GovUkHomePage govUkHomePage;

    @Step("Open the Gov Uk home page")
    public void govUkHomePage() {
        govUkHomePage.open();
    }


    @Step("Click first result")
    public void govUkClickFirstResult() {
        $(NavigationSelectors.CLICK_FIRST_RESULT).click();
    }

    @Step("Start search")
    public void govUkSelectStartSearch() {
        $(NavigationSelectors.SEARCH_CLICK).click();
    }


    @Step("Select next step")
    public void govUkSelectNextStep() {
        $(NavigationSelectors.SELECT_NEXT_STEP).click();
    }


    @Step("Get decision")
    public String govUkGetDecision() {
        return $(NavigationSelectors.GET_DECISION).getText();
    }


}
