package gov.nationality;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;

public class Nationality extends UIInteractionSteps {

    @Step("Select nationality")
    public void govUkSelectNationality(String nationality) {
        $(NationalitySelectors.SELECT_NATIONALITY_DROPDOWN).click();
        $(NationalitySelectors.SELECT_NATIONALITY_DROPDOWN).selectByVisibleText(nationality);
// Previously used selectByValue this has stopped worked now
    }
}
