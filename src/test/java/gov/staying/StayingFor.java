package gov.staying;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;

public class StayingFor extends UIInteractionSteps {

    @Step("Select length of stay")
    public void govUkSelectLengthOfStay(int length) {
        if (length <= 6) {

            $(StayingSelectors.SELECT_SIX_MONTHS_OR_LESS).click();

        } else {
            $(StayingSelectors.SELECT_OPTION_TWO).click();

        }
    }
}
