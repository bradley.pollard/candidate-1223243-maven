@api
Feature: Query a postcode and receive a 200 response

  Scenario: Send request to api.postcodes.io and check response
    When I send a get request to "SW1P4JA" and get a "200" response