#!/bin/bash

#Installs docker compose and runs docker compose
#Evaluates the status of the Selenium Grid
#Runs tests on Chrome and Firefox
#Opens the Zalenium Dashboard


sudo curl -L "https://github.com/docker/compose/releases/download/1.26.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

docker-compose up -d

statusCode=$(curl -s -o /dev/null -w '%{http_code}' http://localhost:4444/wd/hub/status)

if [[ $statusCode -ne 400 ]]
then
echo We\'re not ready!!
sleep 20
fi

curl http://localhost:4444/dashboard/cleanup?action=doReset

#mvn verify -Dwebdriver.remote.url=http://localhost:4444/wd/hub -Dwebdriver.remote.driver=firefox

mvn clean verify -Dwebdriver.remote.url=http://localhost:4444/wd/hub -Dwebdriver.remote.driver=chrome

google-chrome 'http://localhost:4444/dashboard/#'
google-chrome '../candidate-1223243-maven/target/site/serenity/index.html'